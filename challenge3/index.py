def nesteddict(object,keys):
    if not isinstance(keys,list):
        raise ValueError("Invalid Key")
    if not isinstance(object,dict):
        raise ValueError("Invalid object")
    if keys == []:
        return None
    current = object
    for key in keys:
        if key in current:
            current = current[key]
        else:
            return None
    
    return current
'''
obj = {"a":{"b":{"c":"d"}}}
keysam = []

print(nesteddict(obj,keysam))
'''
