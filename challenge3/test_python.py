import unittest
from index import nesteddict

class Testdict(unittest.TestCase):

    def test_dict(self):
        object = {"a":{"b":{"c":"d"}}}

        # Test case 1 Should return value d as expected
        result1 = nesteddict(object,['a','b','c'])
        self.assertEqual(result1,'d')

        # Test case 2 Should return value {'c':'d'}
        result2 = nesteddict(object,['a','b'])
        self.assertEqual(result2,{'c':'d'})

        # Test case 3 Sould return {"b":{"c":"d"}}
        result3 = nesteddict(object,["a"])
        self.assertEqual(result3,{"b":{"c":"d"}})

        # Test case 4 Should return none        
        result4 = nesteddict(object,[])  
        self.assertEqual(result4,None)

        # Test case 5 Should return none
        result5 = nesteddict(object,['x']) 
        self.assertEqual(result4,None)
        
        # should return error if invalid object or Keys
        self.assertRaises(ValueError,nesteddict,object,'a')
        self.assertRaises(ValueError,nesteddict,["a",{"b":{"c":"d"}}],'a')


if __name__ == '__main__':
    unittest.main()